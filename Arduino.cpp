#include "Arduino.h"

VirtualSerial Serial;

VirtualSerial::VirtualSerial(){}

void VirtualSerial::print(const unsigned char i, int radix)
{
    switch(radix)
    {
    case 0:
        printf("%c", i);
        break;
    case 10:
        printf("%u", i);
        break;
    case 16:
        printf("%X", i);
        break;
    default:
        printf("PRINT_UNSUPPORTED_RADIX");
        break;
    }
}

void VirtualSerial::print(const char *s)
{
    printf("%s",s);
}

void VirtualSerial::print(const unsigned char *s)
{
    printf("%s",s);
}

void VirtualSerial::println()
{
    printf("\n");
}

void VirtualSerial::println(const unsigned char i, int radix)
{
    switch(radix)
    {
    case 0:
        printf("%c\n", i);
        break;
    case 10:
        printf("%u\n", i);
        break;
    case 16:
        printf("%X\n", i);
        break;
    default:
        printf("PRINT_UNSUPPORTED_RADIX\n");
        break;
    }
}

void VirtualSerial::println(const char *s)
{
    printf("%s\n",s);
}

void VirtualSerial::println(const unsigned char *s)
{
    printf("%s\n",s);
}
