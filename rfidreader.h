#ifndef RFIDREADER_H
#define RFIDREADER_H

#include <MFRC522.h>
#include <avr/pgmspace.h>
#include <stdint.h>

struct RfidTag
{
public:
    static constexpr uint8_t labelStringMaxLen = 17;
    uint32_t id;
    uint8_t groupMask;
    const char label[labelStringMaxLen];
};

class RfidReader
{
public:

    enum Status : uint8_t
    {
        Status_NoReader = 0, Status_NoTag, Status_UnsupportedTag,
        Status_TagAllowed, Status_TagRejected,
        STATUS_COUNT    // NOT AN ACTUAL STATUS
    };

    RfidReader(int pinSS, int pinRST);
    void setupPins();
    void setKnownTags(const RfidTag allowedTagsArray[], uint8_t groupMask);
    void begin();

    void poll();

    Status getPrevStatus();
    Status getStatus();
    uint32_t getTagId();
    RfidTag *getMatchingTagPtr();
    bool hasSomethingChanged();

    uint8_t copyStatusStr(char* dest);
    constexpr static uint8_t maxStatusStrLength()
        {return s_statusStringMaxLen + RfidTag::labelStringMaxLen + 20;}

private:
    bool tryToWakeUpPICC();
    bool tryToReadAKnownRegister();
    bool lookForKnownTagMatchingId();
    uint8_t copyStringFromFlashHelper(char* dst, const char* src, uint8_t maxSize);
    uint8_t copyTagIdToStringHelper(char* dst);

    int m_pinSS;
    int m_pinRST;
    MFRC522 m_mfrc522;

    Status m_prevStatus;
    Status m_currStatus;

    uint32_t m_currTagId;
    uint32_t m_prevTagId;

    RfidTag* m_matchingTagPtr;

    const RfidTag* m_allowedTagsArray;
    uint8_t m_groupMask;

    static constexpr uint8_t s_statusStringMaxLen = 17;

    static const char s_nibbleToChar[16];
    static const char s_unknownTagString[RfidTag::labelStringMaxLen];
    static const char s_statusStringArray[STATUS_COUNT][s_statusStringMaxLen];
};

#endif
