#ifndef ARDUINO_H
#define ARDUINO_H

#include <wiringPi.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define PROGMEM
#define F(X) X
#define DEC 10
#define HEX 16
typedef uint8_t byte;
typedef char __FlashStringHelper;


class VirtualSerial
{
public:
    VirtualSerial();

    void print(const unsigned char i, int radix = 0);
    void print(const char* s);
    void print(const unsigned char* s);
    void println(void);
    void println(const unsigned char i, int radix = 0);
    void println(const char* s);
    void println(const unsigned char* s);
};

extern VirtualSerial Serial;

inline unsigned char pgm_read_byte(const unsigned char* a)
{
    return (*a);
}

#endif
