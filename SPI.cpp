#include "SPI.h"
#include <wiringPiSPI.h>
#include <unistd.h>

#define SPI_CHANNEL 1   //Big hack, by using the channel 1, CE0 is actually free for us to use properly
#define SPI_CLOCK   1000000

SPIClass SPI;

SPISettings::SPISettings(int, int, int)
{

}

void SPIClass::beginTransaction(const SPISettings &settings)
{
    //Currently hardwired channel 0, 1Mhz clock
    spiFd = wiringPiSPISetup(SPI_CHANNEL, SPI_CLOCK);
//    printf("Open SPI FD : %u\n",spiFd);
}

byte SPIClass::transfer(byte b)
{
    byte c = b;
    wiringPiSPIDataRW(SPI_CHANNEL, &c, 1);
//    printf("Written : 0x%X, read : 0x%X\n", b, c);
    return c;
}

void SPIClass::endTransaction()
{
    close(spiFd);
    spiFd = -1;
//    printf("Close SPI FD\n");
}
