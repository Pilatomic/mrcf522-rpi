#include "rfidreader.h"

constexpr uint8_t TAG_UID_SIZE  = 4;

static const char RfidReader::s_nibbleToChar[16] PROGMEM =
{
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
    'A', 'B', 'C', 'D', 'E', 'F'
};

static const char RfidReader::s_unknownTagString[RfidTag::labelStringMaxLen] PROGMEM =
        "Tag Inconnu";

static const char RfidReader::s_statusStringArray[STATUS_COUNT][s_statusStringMaxLen] PROGMEM =
{
    "Lecteur HS",
    "Tag absent",
    "Tag incompatible",
    "Tag autorise",
    "Tag refuse"
};

RfidReader::RfidReader(int pinSS, int pinRST) :
    m_pinSS(pinSS),
    m_pinRST(pinRST),
    m_mfrc522(m_pinSS,m_pinRST),
    m_prevStatus(Status_NoReader),
    m_currStatus(Status_NoReader),
    m_prevTagId(0x00000000),
    m_currTagId(0x00000000),
    m_matchingTagPtr(nullptr),
    m_allowedTagsArray(nullptr),
    m_groupMask(0x00)
{

}

void RfidReader::setupPins()
{
    pinMode(m_pinRST, OUTPUT);
    digitalWrite(m_pinRST, LOW);

    pinMode(m_pinSS, OUTPUT);
    digitalWrite(m_pinSS, HIGH);
}

void RfidReader::setKnownTags(const RfidTag allowedTagsArray[], uint8_t groupMask)
{
    m_allowedTagsArray = allowedTagsArray;
    m_groupMask = groupMask;
    m_matchingTagPtr = nullptr;
}

void RfidReader::begin()
{
    m_mfrc522.PCD_Init();
}

void RfidReader::poll()
{
    m_prevStatus = m_currStatus;
    m_prevTagId = m_currTagId;
    m_currTagId = 0x00000000;
    m_matchingTagPtr = nullptr;

    // Init reader if required
    if(m_prevStatus == Status_NoReader || !tryToReadAKnownRegister())
    {
        begin();
        if(!tryToReadAKnownRegister())
        {
            m_currStatus = Status_NoReader;
            return;
        }
    }

    //Try to read card
    if(!tryToWakeUpPICC() || !m_mfrc522.PICC_ReadCardSerial())
    {
        m_currStatus = Status_NoTag;
        return;
    }

    //Now we don't need communication any more
    m_mfrc522.PICC_HaltA();
    m_mfrc522.PCD_StopCrypto1();

//    //DEBUG
//    for (byte i = 0; i < m_mfrc522.uid.size; i++) {
//        Serial.print(m_mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
//        Serial.print(m_mfrc522.uid.uidByte[i], HEX);
//    }
//    Serial.print(F(", "));
//    MFRC522::PICC_Type piccType = m_mfrc522.PICC_GetType(m_mfrc522.uid.sak);
//    Serial.print(m_mfrc522.PICC_GetTypeName(piccType));
//    //DEBUG

    //check UID size
    if(m_mfrc522.uid.size != TAG_UID_SIZE)
    {
        m_currStatus = Status_UnsupportedTag;
        return;
    }

    //Store UID
    for(uint8_t i = 0 ; i < TAG_UID_SIZE ; i++)
    {
        m_currTagId <<= 8;
        m_currTagId |= m_mfrc522.uid.uidByte[i];
    }

    //Check if this tag is allowed
    if( lookForKnownTagMatchingId()
    &&( pgm_read_byte_near(&m_matchingTagPtr->groupMask) & m_groupMask ) )
    {
        m_currStatus = Status_TagAllowed;
        return true;
    }
    m_currStatus = Status_TagRejected;
}

bool RfidReader::lookForKnownTagMatchingId()
{
    if(m_allowedTagsArray != nullptr && m_groupMask != 0x00)
    {
        m_matchingTagPtr = m_allowedTagsArray;
        while(1)
        {
            uint32_t allowedTagId = pgm_read_dword_near(&m_matchingTagPtr->id);
            if(allowedTagId == 0x00000000) break;   //Terminator found
            if(allowedTagId == m_currTagId) return true;
            m_matchingTagPtr++;
        }
    }
    m_matchingTagPtr = nullptr;
    return false;
}



RfidReader::Status RfidReader::getPrevStatus()
{
    return m_prevStatus;
}

RfidReader::Status RfidReader::getStatus()
{
    return m_currStatus;
}

uint32_t RfidReader::getTagId()
{
    return m_currTagId;
}

RfidTag *RfidReader::getMatchingTagPtr()
{
    return m_matchingTagPtr;
}

bool RfidReader::hasSomethingChanged()
{
    return (m_currStatus != m_prevStatus) || (m_currTagId != m_prevTagId);
}

uint8_t RfidReader::copyStatusStr(char *dest)
{
    unsigned char c = 0xFF;
    uint8_t i = 0;

    // STATUS
    i += copyStringFromFlashHelper(&dest[i],
                                   s_statusStringArray[m_currStatus],
                                   s_statusStringMaxLen);

    //Only status copied if no tag data are available
    if(m_currStatus != Status_TagAllowed && m_currStatus != Status_TagRejected)
    {
        dest[i++] = '\0';
        return i;
    }

    // SEPARATOR
    i--;
    dest[i++] = ' ';
    dest[i++] = ':';
    dest[i++] = ' ';


    // TAG LABEL
    i += copyStringFromFlashHelper(&dest[i],
                                   m_matchingTagPtr ? m_matchingTagPtr->label : s_unknownTagString,
                                   RfidTag::labelStringMaxLen);

    i--;
    dest[i++] = ' ';
    dest[i++] = '(';
    i += copyTagIdToStringHelper(&dest[i]);
    dest[i++] = ')';
    dest[i++] = '\0';

    return i;   // Return number of byte copied
}

uint8_t RfidReader::copyStringFromFlashHelper(char *dst, const char *src, uint8_t maxSize)
{
    uint8_t srcLen = strlcpy_P(dst, src, maxSize);
    return (srcLen < maxSize) ? srcLen+1 :  maxSize-1;
}

uint8_t RfidReader::copyTagIdToStringHelper(char *dst)
{
    for(uint8_t i = 0 ; i < TAG_UID_SIZE ; i++)
    {
        if(i != 0) *dst++ = ' ';
        uint8_t shiftAmount = (TAG_UID_SIZE-1-i) * 8;
        uint8_t currByte = m_currTagId >> shiftAmount;
        *dst++ = pgm_read_byte_near(&s_nibbleToChar[currByte >> 4]);
        *dst++ = pgm_read_byte_near(&s_nibbleToChar[currByte & 0x0F]);
    }
    return TAG_UID_SIZE*2 + TAG_UID_SIZE-1;
}



bool RfidReader::tryToWakeUpPICC()
{
    byte bufferATQA[2];
    byte bufferSize = sizeof(bufferATQA);

    // Reset baud rates
    m_mfrc522.PCD_WriteRegister(MFRC522::TxModeReg, 0x00);
    m_mfrc522.PCD_WriteRegister(MFRC522::RxModeReg, 0x00);
    // Reset ModWidthReg
    m_mfrc522.PCD_WriteRegister(MFRC522::ModWidthReg, 0x26);

    MFRC522::StatusCode result = m_mfrc522.PICC_WakeupA(bufferATQA, &bufferSize);
    return (result == MFRC522::STATUS_OK || result == MFRC522::STATUS_COLLISION);
}

bool RfidReader::tryToReadAKnownRegister()
{
    return m_mfrc522.PCD_ReadRegister(MFRC522::ModWidthReg) == 0x26;
}
