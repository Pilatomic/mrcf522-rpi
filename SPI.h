#ifndef SPI_H
#define SPI_H

#include <stdint.h>
#include "Arduino.h"
#define SPI_CLOCK_DIV4  0
#define MSBFIRST        0
#define SPI_MODE0       0

class SPISettings {
public:
    SPISettings(int, int, int);
};


class SPIClass {
public:
    void beginTransaction(const SPISettings &settings);
    byte transfer(byte b);
    void endTransaction();
private:
    int spiFd = -1;
};

extern SPIClass SPI;

#endif
