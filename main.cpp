#include <stdio.h>
#include <stdint.h>
#include <wiringPi.h>
#include <unistd.h>
#include "mfrc522/MFRC522.h"

#define PIN_RST 25
#define PIN_CS  8

constexpr uint8_t TAG_UID_SIZE  = 4;


bool tryToReadAKnownRegister(MFRC522* mfrc)
{
    return mfrc->PCD_ReadRegister(MFRC522::ModWidthReg) == 0x26;
}


bool tryToWakeUpPICC(MFRC522* mfrc)
{
    uint8_t bufferATQA[2];
    uint8_t bufferSize = sizeof(bufferATQA);

    // Reset baud rates
    mfrc->PCD_WriteRegister(MFRC522::TxModeReg, 0x00);
    mfrc->PCD_WriteRegister(MFRC522::RxModeReg, 0x00);
    // Reset ModWidthReg
    mfrc->PCD_WriteRegister(MFRC522::ModWidthReg, 0x26);

    MFRC522::StatusCode result = mfrc->PICC_WakeupA(bufferATQA, &bufferSize);
    return (result == MFRC522::STATUS_OK || result == MFRC522::STATUS_COLLISION);
}


int main()
{
    wiringPiSetupGpio();

    MFRC522 mfrc(PIN_CS,PIN_RST);
    mfrc.PCD_Init();

    if(!tryToReadAKnownRegister(&mfrc))
    {
        printf("Reader Error\n");
        return -1;
    }

    if(!tryToWakeUpPICC(&mfrc))
    {
        printf("No card\n");
        return 0;
    }

    if(!mfrc.PICC_ReadCardSerial())
    {
        printf("No Serial\n");
        return 0;
    }

    mfrc.PICC_HaltA();
    mfrc.PCD_StopCrypto1();

    if(mfrc.uid.size != TAG_UID_SIZE)
    {
        return 0;
    }

    //Store UID
    for(uint8_t i = 0 ; i < TAG_UID_SIZE ; i++)
    {
        uint8_t tagByte = mfrc.uid.uidByte[i];
        printf("%X", tagByte);
    }
    printf("\n");

    return 1;
}
