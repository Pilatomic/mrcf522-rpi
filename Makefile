# define some Makefile variables for the compiler and compiler flags
# to use Makefile variables later in the Makefile: $()
#
#  -g    adds debugging information to the executable file
#  -Wall turns on most, but not all, compiler warnings
#
# for C++ define  CC = g++
CC = g++
CFLAGS  = -g -Wall -lwiringPi

# typing 'make' will invoke the first target entry in the file 
# (in this case the default target entry)
# you can name this target entry anything, but "default" or "all"
# are the most commonly used names by convention
#
default: rfid

#count:  countwords.o counter.o scanner.o 
#	$(CC) $(CFLAGS) -o count countwords.o counter.o scanner.o

rfid: MFRC522.o MFRC522Debug.o Arduino.o SPI.o main.o
	$(CC) $(CFLAGS) -o rfid MFRC522.o MFRC522Debug.o Arduino.o SPI.o main.o

main.o: main.cpp
	$(CC) $(CFLAGS) -I . -c main.cpp

MFRC522.o: mfrc522/MFRC522.cpp mfrc522/MFRC522.h
	$(CC) $(CFLAGS) -I . -c mfrc522/MFRC522.cpp

MFRC522Debug.o: mfrc522/MFRC522Debug.cpp mfrc522/MFRC522Debug.h
	$(CC) $(CFLAGS) -I . -c mfrc522/MFRC522Debug.cpp

Arduino.o: Arduino.cpp Arduino.h
	$(CC) $(CFLAGS) -c Arduino.cpp

SPI.o: SPI.cpp SPI.h
	$(CC) $(CFLAGS) -c SPI.cpp

clean: 
	$(RM) -r count *.o *~
